create database millennium;
use millennium;

create table usuarios(
	id int auto_increment,
	nombre varchar(50) unique not null,
	apellido varchar(50) unique not null,
	primary key(id)
);


function agregardatos(nombre,apellido){

	if (nombre != "") {
		if (apellido != "") {

		cadena="nombre=" + nombre + "&apellido=" + apellido;

		$.ajax({
			type:"POST",
			url:"php/agregarDatos.php",
			data:cadena,
			success:function(r){
				if(r==1){
					$('#tabla').load('componentes/tabla.php');
					 $('#buscador').load('componentes/buscador.php');
					alertify.success("Agregado con exito.");
				}else{
					alertify.error("Falló el servidor al agregar.");
				}
			}
		});
		} else {
			alertify.error("Fallo al guardar, campo APELLIDO no puede estar vacío.");
		}
	} else {
		alertify.error("Fallo al guardar, campo NOMBRE no puede estar vacío.");
	}

}

function agregaform(datos){

	d=datos.split('||');

	$('#idpersona').val(d[0]);
	$('#nombreu').val(d[1]);
	$('#apellidou').val(d[2]);
	
}

function actualizaDatos(){


	id=$('#idpersona').val();
	nombre=$('#nombreu').val();
	apellido=$('#apellidou').val();

	cadena= "id=" + id + "&nombre=" + nombre + "&apellido=" + apellido;

	if (nombre != "") {
		if (apellido != "") {

			$.ajax({
				type:"POST",
				url:"php/actualizaDatos.php",
				data:cadena,
				success:function(r){
					
					if(r==1){
						$('#tabla').load('componentes/tabla.php');
						alertify.success("Actualizado con exito.");
					}else{
						alertify.error("Falló el servidor al actualizar.");
					}
				}
			});

		} else {
			alertify.error("Fallo al actualizar, campo APELLIDO no puede estar vacío.");
		}
	} else {
		alertify.error("Fallo al actualizar, campo NOMBRE no puede estar vacío.");
	}

}

function preguntarSiNo(id){
	alertify.confirm('Eliminar Datos', '¿Esta seguro de eliminar este registro?', 
					function(){ eliminarDatos(id) }
                , function(){ alertify.error('Se canceló correctamente.')});
}

function eliminarDatos(id){

	cadena="id=" + id;

		$.ajax({
			type:"POST",
			url:"php/eliminarDatos.php",
			data:cadena,
			success:function(r){
				if(r==1){
					$('#tabla').load('componentes/tabla.php');
					alertify.success("¡Eliminado con exito!");
				}else{
					alertify.error("Falló el servidor al eliminar.");
				}
			}
		});
}